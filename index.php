<!DOCTYPE html>

<?php require './header.php'; ?>

<html>

<head>
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
    <?php


    // Program do zarządzania parkingiem. Posiada dwie klasy
    // Klasa ,,Parking" przyjmuje trzy parametry ('nazwa', cena za parking w złotówkach, ilość miejsca na samochody)
    // Metody klasy ,,Parking"
    // parkVehicle (używana jest w klasie Car)
    // unparkVehicle - usuwa samochó z parkingu
    // Klasa ,,Car" przyjmuje dwa parametry ('Nazwa samochodu', czas na jaki auto zostanie zaparkowane)
    // Park - Parkuje auto na konretnym parkingu

    // Program ma zawierać jeszcze kalkulator opłat podczas wyjeżdzania z parkingu

    $parking1 = new Parking('Kosciuszki', 20, 10);
    $parking2 = new Parking('Dabrowskiego', 10, 5);

    $car1 = new Car('Mercedes', 4);
    $car2 = new Car('Honda', 2);
    $car3 = new Car('Toyota', 8);
    $car4 = new Car('Ford', 24);
    $car5 = new Car('Dacia', 1);

    $car5->park($parking2);
    $parking1->unparkVehicle($car2);
    $car1->park($parking2);


    ?>




</body>

</html>
